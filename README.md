# fis-consultoria-de-software-ii

----
![Logotipo](imagenes/simpat-tech.png)
## :computer: SIMPAT TECH 

***Sitio web***

:point_right: [SIMPAT TECH](https://simpat.tech)


***Ubicación***:mag:

Dirección: Ave. Eugenio Garza Sada 3820 Torre Micrópolis, Piso 8, Más Palomas (Valle de Santiago), 64860 Monterrey, N.L.

:point_right: [Google Maps](https://www.google.com/maps/place/Simpat/@25.6385532,-100.2882339,14z/data=!4m5!3m4!1s0x0:0x84a751188693a17d!8m2!3d25.6340798!4d-100.2821369)



***Acerca de***:memo: 

**Simpat** es una empresa de consultoría de software personalizado con sede en Austin, Texas, con un centro de entrega de software de última generación en Monterrey, México, formado por un equipo de profesionales de software innovadores que se esfuerzan por resolver los desafíos y la misión más complejos de nuestros clientes. metas críticas. A través de nuestra experiencia de alto nivel y visión estratégica, creamos experiencias de software exitosas, simples y manejables.


***Servicios***

* Desarrollo, implementación, recursos y consultoría de software contratado y bajo demanda para organizaciones de todo tipo y tamaño.

* Servicios de consultoría y mejora del equipo que mejoran el desempeño organizacional al tiempo que brindan a los clientes una mayor flexibilidad y reducen su riesgo.


***Presencia***

:earth_americas: [Facebook](https://www.facebook.com/simpat.tech)

:earth_americas: [Instagram](https://www.instagram.com/simpat.tech)

:earth_americas: [Twitter](https://twitter.com/simpat_tech)

:earth_americas: [Linkedin](https://www.linkedin.com/company/simpattech/)


***Ofertas laborales***

Se puede aplicar para loborar en la empresa en https://simpat.tech/careers/ o enviando tu curriculum al siguiente correo people@simpat.tech


***Blog de Ingeniería***

Para ingresar al [Blog](https://simpat.tech/insights/) :point_left:


***Tecnologías***

* Custom Software

    - Gold Certified Microsoft Partner for Application Development
    - Development methodology, including Agile and Kanban scrum
    - Microsoft Stack, including .NET Core, ASP .NET MVC, API, SQL, Entity framework, and C#
    - Design patterns, including mediator and dependency Injection
    - Mobile development, including ReactNative and Xamarin
    - Front-end development, including ReactJS, Angular, and CSS/SASS/CSS Grid
    - Containers, including Docker


* DevOps

    - Silver Certified Microsoft Partner in DevOps
    - CI/CD, including Azure pipeline, Kubernetes, Docker, Octopus, Application Insights, and Logging


* Cloud Solutions

    - Azure
    - AWS
    - Kubernetes
    - Docker
    - PAaS
    - RabbitMQ
    - Microservices
    - Cloud services

----

----
![Logotipo](imagenes/aviada.PNG)
## :computer: AVIADA 


***Sitio web***

:point_right: [AVIADA](https://simpat.tech)

***Ubicación***:mag:

Dirección: Bulevar Luis D. Colosio 671-12vo Piso, Santa Fe, 83249 Hermosillo, Son.

:point_right: [Google Maps](https://www.google.com/maps/place/Aviada/@29.0859297,-110.9962835,15z/data=!4m2!3m1!1s0x0:0x486fad3c91dd41c0?sa=X&ved=2ahUKEwjOr_S45sjzAhWykmoFHXpRCiEQ_BJ6BAhkEAU)

***Acerca de***:memo: 

Somos una empresa 100% mexicana fundada en 2017, hecha por empleados para empleados, comprometidos con hacer siempre lo correcto y lo justo. Nuestros clientes (empresas responsables y bien establecidas) se encuentran principalmente en California y sus actividades son muy variadas: Entretenimiento, Publicidad, Compras Online, etc.

***Servicios***

* IT Resouces, Software Development, Quality Assurance, Dev Ops, Tech Ops, Software Engineers y Client Services

***Presencia***

:earth_americas: [Facebook](https://www.facebook.com/aviadamx)

:earth_americas: [Instagram](https://www.instagram.com/aviadamx/)

:earth_americas: [Twitter](https://twitter.com/AviadaMX)

:earth_americas: [Linkedin](https://www.linkedin.com/company/aviadamx/)


***Ofertas laborales***

Se puede aplicar para loborar en la empresa en https://careers.aviada.mx


***Blog de Ingeniería***

Para ingresar al [Blog](https://aviada.mx/category/blog/) :point_left:


***Tecnologías***

- Account Management
- Ad Ops / Trafficking
- Ad Bidding
- AMP
- Data Science
- DevOps
- E-Commerce
- Javascript
- Joomla CMS
- Laravel
- NodeJS
- PHP
- Python


----

---

![Logotipo](imagenes/estrasol.PNG)
## :computer: ESTRASOL 

***Sitio web***

:point_right: [ESTRASOL](https://estrasol.com.mx/)


***Ubicación***:mag:

Dirección: S. Pedro S 105, Col. del Valle, Del Valle, 66220 San Pedro Garza García, N.L.


:point_right: [Google Maps](https://www.google.com/maps/place/Estrasol/@25.6584669,-100.3695011,15z/data=!4m2!3m1!1s0x0:0xde8c49bda1b95c8d?sa=X&ved=2ahUKEwi6qseD68jzAhU0k2oFHXnNBeMQ_BJ6BAhQEAU)


***Acerca de***:memo: 

Somos una empresa sólida que ofrece una estrategia de solución de 360º con unidades de negocio especializada en Consultoría, Desarrollo de Software y Marketing Digital.



***Servicios***

* En Estrasol, contamos con las herramientas y procesos necesarios para innovar, a través de nuestros expertos en Marketing Digital, CRM, ERP, ODOO, Diseño y Desarrollo Web, Apps y Kioscos Digitales.


***Presencia***

:earth_americas: [Facebook](https://www.facebook.com/Estrasol/)

:earth_americas: [Instagram](https://www.instagram.com/aviadamx/)

:earth_americas: [Twitter]:  https://www.twitter.com/estrasol_

:earth_americas: [Linkedin](https://www.linkedin.com/company/estrasol/?originalSubdomain=mx)

:earth_americas: [Behance](https://www.behance.net/estrasol)


***Ofertas laborales***

Se puede aplicar para loborar en la empresa en https://estrasol.com.mx/unete-a-estrasol

***Blog de Ingeniería***

Para ingresar al [Blog](https://estrasol.com.mx/blog) :point_left:

***Tecnologías***

- Java
- Swift
- Xamarin
- React
- Php
- Boosttrap
- HTML

----

---
![Logotipo](imagenes/tacit.png)
## :computer: Tacit Knowledge Guadalajara 

***Sitio web***

:point_right: [TACIT KNWLEDGE](https://www.tacitknowledge.com/)


***Ubicación***:mag:

Dirección: Av. de las Américas 1545-Piso 7, Providencia, 44630 Guadalajara, Jal.


:point_right: [Google Maps](Av. de las Américas 1545-Piso 7, Providencia, 44630 Guadalajara, Jal.)


***Acerca de***:memo: 

Tacit Knowledge desarrolla, integra y respalda software empresarial para marcas conocidas y minoristas. Ofrecemos soluciones en todos los puntos de contacto del consumidor, desde el dispositivo hasta la puerta. Conectamos todo el ecosistema del negocio de comercio digital de nuestros clientes. Creamos y apoyamos el sitio web de comercio electrónico y las tecnologías asociadas a través del suministro de cumplimiento (Pick & Pack, Kitting ...) hasta las herramientas de entrega, devoluciones y participación del cliente posteriores a la compra.

***Servicios***

* Consultaría de software y ecommerce
* Comercio digital
* Comercio SAP
* Servicios gestionados
* Ncommerce

***Presencia***

:earth_americas: [Facebook](https://es-la.facebook.com/TacitGDL/)

:earth_americas: [Twitter](https://twitter.com/TacitGDL)

:earth_americas: [Linkedin](https://www.linkedin.com/company/tacit-knowledge/)


***Ofertas laborales***

Se puede aplicar para loborar en la empresa en https://www.tacitknowledge.com/careers

***Blog de Ingeniería***

Para ingresar al [Blog](https://www.tacitknowledge.com/thoughts/) :point_left:


***Tecnologías***

- AWS/GCP/Azure
- Cloud & DevOps
- Data bases : MySQL, MSSQL, PostgreSQL
- Languages such as Ruby, Python
- Web technologies


----

![Logotipo](imagenes/tango.PNG)
## :computer:  TANGO

***Sitio web***

:point_right: [TANGO](https://tango.io/)


***Ubicación***:mag:

Dirección: Tango Avenida La Paz 51, Villas Paraíso, 28079 Colima, México

:point_right: [Google Maps](https://www.google.com/maps/search/Tango+Avenida+La+Paz+51,+Villas+Paraíso,+28079+Colima,+México/@19.2649955,-103.715743,17z/data=!3m1!4b1?hl=es)


***Acerca de***:memo: 

Desarrollamos software para soluciones interactivas y picantes. Ya sea que sea una empresa nueva o una corporación, podemos ayudarlo a transformar grandes ideas en software utilizable para productos atractivos.


***Servicios***

- Producto mínimo viable 
- Aumento de personal 
- Technical debt resolution 
- Estrategia de producto 
- Escalabilidad y aceleración del producto


***Presencia***

:earth_americas: [Facebook](https://www.facebook.com/Tango.ioMX)

:earth_americas: [Instagram](https://www.instagram.com/tangodotio/)

:earth_americas: [Twitter](https://twitter.com/tango_io)

:earth_americas: [Linkedin](https://www.linkedin.com/company/tango-io/)

***Ofertas laborales***

Se puede aplicar para loborar en la empresa en https://jobs.lever.co/tango

***Blog de Ingeniería***

Para ingresar al [Blog](https://blog.tango.io) :point_left:


***Tecnologías***

- DevOps
    - Jenkins Terraform Docker Heroku
    
- UX/UI
    - Adobe XD Photoshop Illustrator Sketch Invision

- WebDev
    - JavaScript React HTML CSS Ruby on Rails Node

- Mobile
    - React Native Java Kotlin Swift Ionic Objective C


----


---

![Logotipo](imagenes/encuresyst.PNG)
## :computer: ENROUTE SYSTEMS

***Sitio web***

:point_right: [ENROUTE SYSTEMS](http://www.enroutesystems.com/)


***Ubicación***:mag:

Dirección: Torre Sol Alto, Puerta del Sol 209. Dinastía, Monterrey, N.L. 64639


:point_right: [Google Maps](https://www.google.com.mx/maps/place/TORRE+SOL+ALTO/@25.7010181,-100.3779984,19z/data=!3m1!4b1!4m5!3m4!1s0x86629643f4af513b:0xbcb1a72ae71a4786!8m2!3d25.7010171!4d-100.377459)


***Acerca de***:memo: 

Surgimos de una idea visionaria. Artemio, Javier y Alex comenzaron esta empresa con la idea de crear algo genial: un lugar donde las personas puedan trabajar en lo que aman, conocer gente de diferentes orígenes y aprender nuevas formas de involucrarse con la tecnología.

Brindamos servicios y soluciones de TI proporcionados por un equipo de personas apasionadas en la resolución de problemas, altamente capacitadas en diversas prácticas comerciales y de TI. Ofrecemos a nuestros clientes cinco servicios principales: control de calidad y pruebas, desarrollo de software, gestión de inventario, ingeniería de datos y mantenimiento de aplicaciones.


***Servicios***

* Análisis avanzado y aprendizaje automático
* Desarrollo de software
* Gestión de datos empresariales y transformación digital
* Inteligencia empresarial y visualización de datos
* Migraciones e integraciones de datos
* Modernización y migración de plataforma de datos


***Presencia***

:earth_americas: [Facebook](https://www.facebook.com/enroutesystems)

:earth_americas: [Instagram](https://www.instagram.com/enroutesystems/)

:earth_americas: [Linkedin](https://www.linkedin.com/company/enroutesystems/)

***Ofertas laborales***

* QA Automation Engineer with Python
* Sr. QA Automation Engineer with JavaScript
* Senior .Net Developer
* Full Stack Developer
* Release Manager/DevOps Engineer
* UX Designer
* Scala Developer

Se puede aplicar para loborar en la empresa en https://enroutesystems.com/careers

***Blog de Ingeniería***

Para ingresar al [Blog](https://enroutesystems.com/case-studies) :point_left:


***Tecnologías***

* Analytics, ETL, MapR, PowerBI, SQL
* Master Data Management, Oracle, SAP
* Hadoop, Kafka, MapR, MongoDB, SCADA, SSRS, STreaming
* Android, API, iOS, Javascript, java

----

---

![Logotipo](imagenes/ensitech.PNG)
## :computer: ENSITECH

***Sitio web***

:point_right: [ENSITECH](https://ensitech.com)


***Ubicación***:mag:

Direcciónes: 
* Ave. Juárez #1102, Local N5A Col. Centro ​Monterrey, Nuevo León, México. ​​

:point_right: [Google Maps](https://www.google.com.mx/maps/search/Ave.+Juárez+%231102,+Local+N5A+Col.+Centro+%E2%80%8BMonterrey,+Nuevo+León,+México.+%E2%80%8B%E2%80%8B/@25.6724355,-100.3130236,13z/data=!3m1!4b1) 

* Paseo de la Reforma 222 Torre 1, Piso 18, Suite 1823 Col. Juárez. Mexico City

:point_right: [Google Maps](https://www.google.com.mx/maps/search/Paseo+de+la+Reforma+222+Torre+1,+Piso+18,+Suite+1823+Col.+Juárez.+Mexico+City/@19.4297622,-99.1628504,17z/data=!3m1!4b1) 


* Edificio Sonora – Oficina 24 2do Nivel Insurgente Pedro Moreno #24. Hermosillo, Sonora

:point_right: [Google Maps](https://www.google.com.mx/maps/search/Edificio+Sonora+–+Oficina+24+2do+Nivel+Insurgente+Pedro+Moreno+%2324.+Hermosillo,+Sonora/@29.0766648,-110.9598737,17z/data=!3m1!4b1)

***Acerca de***:memo: 

Somos un equipo de ingenieros, diseñadores, analistas de negocios y científicos de datos que crean productos enfocados en ayudar a gobiernos y empresas a ser más ágiles y conectados con sus mercados mediante el uso de tecnologías de nube, móviles y Big Data.

Nuestra empresa fue fundada en 2006, por un equipo de estudiantes graduados del Tec de Monterrey, una universidad líder en América Latina. Los fundadores de Ensitech fueron reconocidos entre los 100 mejores emprendedores del noreste de México.

***Servicios***

* Software development

* Big Data & AI

* Staff Augmentation

* eCommerce & Web UX

* Cloud Operations


***Presencia***

:earth_americas: [Facebook](https://www.facebook.com/EnsitechMx/)


:earth_americas: [Linkedin](https://www.linkedin.com/company/ensitech/videos/)


***Ofertas laborales***

Se puede aplicar para loborar en la empresa en https://ensitech.com/careers/

***Blog de Ingeniería***

Para ingresar al [Blog](https://ensitech.com/case-study/) :point_left:


***Tecnologías***
* Frameworks
* python
* C#
* Java
* Android
* aws
* JavaScript


----


---

![Logotipo](imagenes/atalait.PNG)
## :computer: ATALAIT

***Sitio web***

:point_right: [ATALAIT](https://www.atalait.com/)


***Ubicación***:mag:

Dirección: Mario Pani 400 (antes 150) Piso 12, Colonia Lomas de Santa Fe. Del. Cuajimalpa, CP 05300, CDMX.


:point_right: [Google Maps](https://www.google.com.mx/maps/place/Mario+Pani+400-Piso+12,+Lomas+de+Santa+Fe,+Contadero,+Cuajimalpa+de+Morelos,+01219+Ciudad+de+México,+CDMX/@19.3595776,-99.2789817,17z/data=!3m1!4b1!4m5!3m4!1s0x85d200cb13725939:0x88bc2e0d0144db20!8m2!3d19.3595726!4d-99.2767877) 


***Acerca de***:memo: 

Atendemos necesidades de negocio mediante el diseño, implementación y operación de Servicios Administrados de Tecnología, Consultoría, Centros de Procesamiento de Datos y Ciberseguridad.

***Servicios***

* Consultoría de negocios especializada en tecnología
* Habilitación de ambientes tecnológicos altamente complejos

***Presencia***

:earth_americas: [Facebook](https://www.facebook.com/Atalait-594842997338059)

:earth_americas: [Instagram](https://www.instagram.com/atalait_adn/)

:earth_americas: [Twitter](https://twitter.com/Atalait)

:earth_americas: [Linkedin](https://www.linkedin.com/company/atalait/)

***Ofertas laborales***
* Vacantes
    * Especialista jr. de telecomunicaciones 
    * Consultor (a) sr de resilencia
    * Director comercial de desarrollo de negocio de agilidad digital
    * Desarrollador de microservicios

Se puede aplicar para loborar en la empresa en https://www.atalait.com/bolsa-de-trabajo/


***Blog de Ingeniería***

Para ingresar al [Blog](https://www.atalait.com/blog/) :point_left:


***Tecnologías***

* Mainframe/ISO 22301
* MANAGEMENT DRP
* DEVOPS & AGILE
*  ITIL & Data Science Mistic Monitoreo
* Inteligente de Servicios TI
* Java
* JScript
* HTML
* JSon



----


---

![Logotipo](imagenes/serti.PNG)
## :computer: SERTI 

***Sitio web***

:point_right: [SERTI](http://www.serti.com.mx)


***Ubicación***:mag:

Dirección: SERTI, Shakespeare 16, Anzures, 11590 Miguel Hidalgo, CDMX.


:point_right: [Google Maps](https://www.google.com.mx/maps/search/SERTI,+Shakespeare+16,+Anzures,+11590+Miguel+Hidalgo/@19.4266304,-99.177984,17.5z) 


***Acerca de***:memo: 

Somos una empresa con mentalidad de innovación y servicio, generamos soluciones de software en tecnologías de información que ayudan al crecimiento de nuestros clientes.


***Servicios***

* Plataformas WEB
* Desarrollo grafico
* Desarrollo de apps
* E-COMMERCE
* balder


***Presencia***

:earth_americas: [Facebook](https://www.facebook.com/SERTIMX/)

:earth_americas: [Twitter](https://twitter.com/serTI_Mx)

:earth_americas: [Linkedin](https://www.linkedin.com/company/servicios-estrat-gicos-en-tecnolog-as-de-informaci-n/)


***Ofertas laborales***

Se puede aplicar para loborar en la empresa en https://serti.com.mx/bolsa-de-trabajo/


***Blog de Ingeniería***

Para ingresar al [Blog](https://serti.com.mx/serti/) :point_left:


***Tecnologías***

* MySQL
* Java 
* Python 
* C++
* Bootstrap
* Sprign Web
* GlassFish
* Hibernate 


----

---

![Logotipo](imagenes/amk.PNG)
## :computer: AMK TECHNOLOGIES

***Sitio web***

:point_right: [AMK TECHNOLOGIES](http://amk-technologies.com/)


***Ubicación***:mag:

Dirección: Tlacoquemecatl 21, int 101. Col. Tlacoquemecatl del Valle, Del. Benito Juárez. C.P. 03200, CDMX, México.


:point_right: [Google Maps](https://www.google.com.mx/maps/place/Torre+de+especialidadesNúmero,+C.+Tlacoquemecatl+21-int+101,+Tlacoquemecatl+del+Valle,+Benito+Juárez,+03200+Ciudad+de+México,+CDMX/@19.3785205,-99.1783782,17z/data=!3m1!4b1!4m5!3m4!1s0x85d1ff9c7a02271d:0x9afcffdd79e3c2b1!8m2!3d19.3785155!4d-99.1761842) 


***Acerca de***:memo: 

AMK Technologies  es una empresa boutique de tecnología ágil, 100% mexicana que forma parte de Grupo Allié. Reconocida como una de las mejores Tech consulting en México y como una empresa Best Place to Code en 2021.

Respaldada por un equipo de jóvenes profesionales con amplia experiencia en Staffing Especializado y Fábrica de Software. Nuestro esfuerzo está enfocado en establecer alianzas estratégicas con nuestros clientes proporcionando servicios agile integrales para el soporte de su operación y proyección de su negocio.


***Servicios***

* Desarrollo de APPS
* Desarrollo WEB
* Solución de Retails


***Presencia***

:earth_americas: [Facebook](https://www.facebook.com/aviadamx)

:earth_americas: [Instagram](https://www.instagram.com/aviadamx/)

:earth_americas: [Twitter](https://twitter.com/AviadaMX)

:earth_americas: [Linkedin](https://www.linkedin.com/company/aviadamx/)


***Ofertas laborales***

* UX DESIGNER Semi-Senior
* DESARROLLADOR .NET Senior
* DESARROLLADOR .NET Junior

Se puede aplicar para loborar en la empresa en http://amk-technologies.com/#solutions


***Blog de Ingeniería***

Para ingresar al [Blog](https://simpat.tech/insights/) :point_left:


***Tecnologías***

* GitLab
* Java 
* Python 
* C++
* DOCKER
*django


----


---

![Logotipo](imagenes/scio.PNG)
## :computer: Scio Consulting 

***Sitio web***

:point_right: [Scio Consulting](http://www.scio.com.mx)


***Ubicación***:mag:

Dirección: Av. Chapultepec Sur 284 – 103 Col. Americana Guadalajara, Jal. 44160

:point_right: [Google Maps](https://www.google.com.mx/maps/place/Av.+Chapultepec+Sur+284-103,+Col+Americana,+Americana,+44160+Guadalajara,+Jal./@20.6717268,-103.3710833,17z/data=!3m1!4b1!4m5!3m4!1s0x8428ae05960fa49f:0x3d5076d2f206a0bb!8m2!3d20.6717218!4d-103.3688893) 


***Acerca de***:memo: 

Somos una empresa de desarrollo de software nearshore y una comunidad de personas apasionadas y orientadas a un propósito. Pensamos de manera disruptiva para ofrecer tecnología para abordar los desafíos más difíciles de nuestros clientes, todo mientras buscamos revolucionar la industria de TI y crear un cambio social positivo.


***Servicios***

* Aplicaciones Web
* Aplicaciones Móviles
* Aplicaciones SaaS
* Consultoría Tecnológica


***Presencia***

:earth_americas: [Facebook](https://www.facebook.com/ScioMx)

:earth_americas: [Twitter](https://twitter.com/sciomx)


***Ofertas laborales***

Se puede aplicar para loborar en la empresa en https://www.scio.com.mx/trabaja-scio-mexico/vacantes/


***Blog de Ingeniería***

Para ingresar al [Blog](https://www.scio.com.mx/blog/) :point_left:


***Tecnologías***
* Trabajamos en las plataformas front-end y back-end más modernas, nos centramos en crear experiencias de usuario interactivas y responsivas basadas en HTML5 / JavaScript que funcionan de manera nativa en los teléfonos celulares y tablets.
* Java 
* Python 



----

---

![Logotipo](imagenes/inteli.PNG)
## :computer: Intelimétrica 

***Sitio web***

:point_right: [SIMPAT TECH](https://intelimetrica.com/)


***Ubicación***:mag:

Dirección: Mariano Escobedo #748, P4 Col. Anzures Alcaldía Miguel Hidalgo CDMX 11590


:point_right: [Google Maps](https://www.google.com.mx/maps/place/Calz.+Gral.+Mariano+Escobedo+748-p4,+Anzures,+Miguel+Hidalgo,+11590+Ciudad+de+México,+CDMX/@19.4254134,-99.1806742,17z/data=!3m1!4b1!4m5!3m4!1s0x85d1ff4dfe67c36f:0xddf9e1090a630985!8m2!3d19.4254084!4d-99.1784802) 


***Acerca de***:memo: 

Intelimetrica es una empresa de desarrollo de productos que aprovecha el aprendizaje automático y la inteligencia artificial para ayudar a sus clientes a transformarse digitalmente y crear ventajas competitivas analíticas fundamentales. Hacemos simple de lo complejo y proporcionamos las herramientas que necesita para convertir la información en conocimiento útil y aplicable. Nuestro ámbito no conoce fronteras ni limitaciones.


***Servicios***

* Ai & ML
* Ingeniería de Software
* Diseño UX


***Presencia***

:earth_americas: [Facebook](https://www.facebook.com/aviadamx)

:earth_americas: [Instagram](https://www.instagram.com/aviadamx/)

:earth_americas: [Twitter](https://twitter.com/AviadaMX)

:earth_americas: [Linkedin](https://www.linkedin.com/company/aviadamx/)

***Ofertas laborales***

Se puede aplicar para loborar en la empresa en https://intelimetrica.com/careers


***Blog de Ingeniería***

Para ingresar al [Blog](https://intelimetrica.com/aws) :point_left:


***Tecnologías***

* SOLID 
* KISS 
* DRY 
* YAGNI
* Python
* Matlab
* Java
* C #
* Sistemas de bases de datos SQL
* Frameworks (Flask, FastAPI, Phoenix)


----


---
 
![Logotipo](imagenes/dotNet.PNG)
## :computer: DotNET Desarrollo de Sistemas

***Sitio web***

:point_right: [DotNET](http://www.cancunit.com/)


***Ubicación***:mag:

Dirección: Carretera Cancun - Aeropuerto Km 7.5


:point_right: [Google Maps](https://www.google.com.mx/maps/place/CancunIT/@21.1054995,-86.8388721,13z/data=!4m9!1m2!2m1!1sCancun+IT+Carretera+Cancun+-+Aeropuerto+Km+7.5!3m5!1s0x8f4c2a36a2d8968b:0xa3ff7b4d7a698064!8m2!3d21.105441!4d-86.838195!15sCi5DYW5jdW4gSVQgQ2FycmV0ZXJhIENhbmN1biAtIEFlcm9wdWVydG8gS20gNy41kgEQc29mdHdhcmVfY29tcGFueQ?authuser=1) 


***Acerca de***:memo: 

DotNET Desarrollo de Sistemas es una empresa 100% mexicana, con más de 13 años de experiencia en desarrollo de software a nivel nacional e internacional.


***Servicios***

* Softlanding: Establecemos oficinas con todo incluido bajo la modalidad softlanding.
* Nearshore: Ponemos a disposición equipos de desarrolladores dedicados nuestros clientes.
* Proyectos a la Media: Proveemos soluciones a la medida en desarrollo de software.


***Presencia***

:earth_americas: [Facebook](https://es-la.facebook.com/dotnetmx/)

:earth_americas: [Linkedin](https://www.linkedin.com/company/dotnet/)

***Ofertas laborales***

* Líder Técnico .NET, Backend, Dockers, Kubernetes
* Líder Técnico REACT

Se puede aplicar para loborar en la empresa en http://www.cancunit.com/#contact


***Blog de Ingeniería***

Para ingresar al [Blog](http://www.cancunit.com/#whatwedo) :point_left:


***Tecnologías***

* Python
* Java
* C #
* HTML5 / JavaScript
----

---

![Logotipo](imagenes/enkoder.PNG)
## :computer: Enkoder 

***Sitio web***

:point_right: [Enkoder](https://enkoder.com.mx/)


***Ubicación***:mag:

Dirección: Ignacio Ramírez 708, Contry La Silla 7o Sector, 67173 Guadalupe, N.L.


:point_right: [Google Maps](https://www.google.com/maps/place/Enkoder/@25.6484285,-100.2684268,17z/data=!4m5!3m4!1s0x8662bf3e21867b51:0x186874c238d9a09d!8m2!3d25.648433!4d-100.2662353) 


***Acerca de***:memo: 


Encuentre e implemente soluciones tecnológicas innovadoras para su negocio con Enkoder.


***Servicios***

* Order Management System
* Delivery Tracking System
* Store Management
* Electronic Invoicing
* Data Warehousing
* Gamification
* eCommerce


***Presencia***

:earth_americas: [Facebook](https://www.facebook.com/Enkoder-Software-Developers-654625184960733/?ref=nearby_places)

:earth_americas: [Twitter](https://twitter.com/EnkoderIT)

:earth_americas: [Linkedin](https://www.linkedin.com/company/enkoder/posts/?feedView=all)


***Ofertas laborales***

* Mid Web Developer
* QA Software Engineer
* DevOps Engineer

Se puede aplicar para loborar en la empresa en https://enkoder.com.mx/contact.html


***Blog de Ingeniería***

Para ingresar al [Blog](https://simpat.tech/insights/) :point_left:


***Tecnologías***
* C#
* PHP
* Objective C
* HTML5/CSS3
* Python
* Java
* Angular
* Xamarin
* .NET Core
* .NET Framework
* Vue
* MySQL


----


---

![Logotipo](imagenes/aptude.PNG)
## :computer: Aptude 

***Sitio web***

:point_right: [Aptude](https://aptude.com/)


***Ubicación***:mag:

Dirección: Rio Rhin 77 8vo piso 06500 Cuauhtémoc, CDMX


:point_right: [Google Maps](https://www.google.com.mx/maps/place/Rio+Rhin+77-8vo+piso,+Cuauhtémoc,+06500+Ciudad+de+México,+CDMX/@19.4299681,-99.1667182,17z/data=!3m1!4b1!4m5!3m4!1s0x85d1ff34e4bc583d:0x253f440f2a14e275!8m2!3d19.4299631!4d-99.1645242?authuser=1) 


***Acerca de***:memo: 

En Aptude, nos esforzamos por ofrecer soluciones técnicas excepcionales y de alta calidad con un servicio al cliente insuperable. Con más de dos décadas de experiencia en tecnología y negocios, estamos personalmente comprometidos a satisfacer las necesidades de nuestros clientes con integridad y completa satisfacción.

Como empresa de consultoría basada en soluciones empresariales, nos asociaremos con usted para implementar software, personal esencial y soluciones de aplicaciones para alcanzar sus objetivos comerciales basados en la tecnología.

***Servicios***

* Servicios de soporte
* Desarrollo de pila completa y microservidores
* Gestión de proyectos
* Personal TI


***Presencia***

:earth_americas: [Facebook](https://www.facebook.com/aptude/)


***Ofertas laborales***

Se puede aplicar para loborar en la empresa en info@aptude.com


***Blog de Ingeniería***

Para ingresar al [Blog](https://aptude.com/es/noticias/) :point_left:


***Tecnologías***

* C#
* PHP
* HTML5/CSS3
* Python
* Java
* MAPR
* hadoop


----

---

![Logotipo](imagenes/nova.PNG)
## :computer: NOVA

***Sitio web***

:point_right: [NOVA](http://novasolutionsystems.com)


***Ubicación***:mag:

Dirección: Liverpool 174, Homework piso 2 Colonia Juárez, 06600 Ciudad De México.

:point_right: [Google Maps](https://www.google.com/maps/place/Nova+Solutions+Systems/@19.4232852,-99.1682266,17z/data=!3m1!4b1!4m5!3m4!1s0x85d2001df411d46d:0xe2d7460a1a13552a!8m2!3d19.4232802!4d-99.1660379?shorturl=1) 


***Acerca de***:memo: 

En Nova nuestra principal experiencia  se basa en fusionar el conocimiento de trabajar con grandes instituciones y la agilidadque se obtiene en fintechs o empresas nativas digitales. “Aceleramos la adopción digital en el sector financiero en México”.


***Servicios***

* Recursos digitales
* Seguridad financiera
* Transformación digital
* Estrategia digital 
* Fintech factory
 


***Presencia***

:earth_americas: [Facebook](https://www.facebook.com/novasolutionsystems/)

:earth_americas: [Instagram](https://www.instagram.com/novasolutionsystems/)

:earth_americas: [Twitter](https://twitter.com/novasolutionsys/)

:earth_americas: [Linkedin](https://www.linkedin.com/company/novasolutionsystems/)


***Ofertas laborales***

* Full Stack Engineer
* Desarrollador Java
* IOS Developer
* ANDROID Developer
* Java Developer Jr

Se puede aplicar para loborar en la empresa en http://novasolutionsystems.com/unete/#4


***Blog de Ingeniería***

Para ingresar al [Blog](http://novasolutionsystems.com/soluciones/#4) :point_left:


***Tecnologías***
* oracle
* C#
* PHP
* Objective C
* HTML5/CSS3
* Python
* Java
* Jenkis
* git
* Angular
* mojito
* Boostrap

----

---
 
![Logotipo](imagenes/ixteco.PNG)
## :computer: Itexico, an Improving company

***Sitio web***

:point_right: [Improving company](https://www.itexico.com/)


***Ubicación***:mag:

Dirección: Avenida patria 888 zapopan jalisco


:point_right: [Google Maps](https://www.google.com.mx/maps/place/Av.+Patria+888,+Jardines+del+Tepeyac,+45034+Zapopan,+Jal./@20.6590314,-103.4258732,17z/data=!3m1!4b1!4m5!3m4!1s0x8428ac20bed41ab3:0xf65d1203e659a118!8m2!3d20.6590264!4d-103.4236792?authuser=1) 


***Acerca de***:memo: 

En iTexico, una empresa que mejora, trabajará con personas de ideas afines de todo el mundo en un entorno divertido similar a Silicon Valley. En lugar de aumentar el estrés y el agotamiento. Creemos en los principios ágiles que crean una experiencia laboral productiva y emocionante.


***Servicios***

* Servicios de desarrollo de software
* Servicios De Ingeniería De Software 
* UI / UX Design
* QA
* Servicios en la nube


***Presencia***

:earth_americas: [Facebook](https://www.facebook.com/itexico)

:earth_americas: [YouTube](https://www.youtube.com/channel/UCuYem4j89E0HbApDJJnyDlw)

:earth_americas: [Twitter](https://twitter.com/improvingmx)

:earth_americas: [Linkedin](https://www.linkedin.com/showcase/improving-nearshore/)



***Ofertas laborales***

* Python Developer
* Front-End Developers
* NET Developers
* Java Developers
* Sr MEAN Full Stack Developers
* DevOps Engineer

Se puede aplicar para loborar en la empresa en https://www.itexico.com/careers#job-opportunities


***Blog de Ingeniería***

Para ingresar al [Blog](https://www.itexico.com/blog) :point_left:


***Tecnologías***

* C#
* PHP
* Objective C
* HTML5/CSS3
* Python
* Java
* React
* Angular
* Vue
* Xamarin
* .NET Core
* .NET Framework
* MySQL
* DynamoDB


----

---

![Logotipo](imagenes/GFT.png)
## :computer: GFT

***Sitio web***

:point_right: [GFT](http://www.gft.com/mx)


***Ubicación***:mag:

Dirección: Gobernador Agustín Vicente Eguia 46, San Miguel Chapultepec I Secc, Miguel Hidalgo, 11850 Ciudad de México, CDMX


:point_right: [Google Maps](https://www.google.com.mx/maps/place/Gobernador+Agustín+Vicente+Eguia+46,+San+Miguel+Chapultepec+I+Secc,+Miguel+Hidalgo,+11850+Ciudad+de+México,+CDMX/@19.4079595,-99.1898317,17z/data=!3m1!4b1!4m5!3m4!1s0x85d1ff5e1780a847:0xc7684076eb498365!8m2!3d19.4079545!4d-99.1876377?authuser=1) 


***Acerca de***:memo: 

GFT es una empresa multinacional de consultoría y proyectos de tecnología de la información. Somos una de las empresas más reconocidas del mundo en proyectos de integración de sistemas para canales digitales, contamos con amplia experiencia en la habilitación de cambios de negocios para resolver los desafíos más críticos de nuestros clientes. Fundada en 1987, GFT contamos con más de 6,000 profesionales en 15 países para poder garantizar la proximidad con nuestros clientes. GFT México opera desde 2015, donde contamos con un equipo de cerca de 400 profesionales.


***Servicios***

* Analisis de datos
* Inteligencia artificial
* Servicios Cloud
* Greencoding



***Presencia***

:earth_americas: [Facebook](https://www.facebook.com/gft.mex/)

:earth_americas: [YouTube](https://www.youtube.com/user/gftgroup/featured)

:earth_americas: [Twitter](https://twitter.com/GFT_Mx)

:earth_americas: [Linkedin](https://www.linkedin.com/company/gft-group/?viewAsMember=true)

:earth_americas: [Medium](https://medium.com/gft-engineering)



***Ofertas laborales***

Se puede aplicar para loborar en la empresa en https://jobs.gft.com/Mexico/go/mexico/4412401/


***Blog de Ingeniería***

Para ingresar al [Blog](https://blog.gft.com/es/) :point_left:


***Tecnologías***
* INTELIGENCIA ARTIFICIAL
* BLOCKCHAIN Y DLT 
* TECNOLOGÍAS CLOUD
* ANÁLISIS DE DATOS
* C#
* PHP
* HTML5/CSS3
* Python
* Java
* Angular
* MySQL
* DynamoDB
* CosmosDB
* SQL Server (SSIS, SSRS)


----


---

![Logotipo](imagenes/lux.PNG)
## :computer: Luxoft

***Sitio web***

:point_right: [Luxoft](https://career.luxoft.com/)


***Ubicación***:mag:

Dirección:  Distrito La Perla, Amado Nervo , 2200 Edificio Bio II piso 1 C.P. 45050, Guadalajara Mexico


:point_right: [Google Maps](https://www.google.com.mx/maps/place/Office+Campus,+La+Perla/@20.6438363,-103.4174082,18z/data=!3m1!4b1!4m5!3m4!1s0x8428ac30a0ede30f:0x31e1cb72a23842da!8m2!3d20.6438008!4d-103.4163655?authuser=1) 


***Acerca de***:memo: 

Luxoft está construido por Luxofters. Cada miembro de nuestro equipo global forma parte de la columna vertebral de nuestro negocio, por lo que nos aseguramos de apoyar el desarrollo continuo de nuestros trabajadores dedicados. Además, construimos equipos altamente efectivos que son incubadoras de logros incomparables.


***Servicios***

* Todo automatizado
* Datos a Insights
* Libro mayor distribuido
* Modernización
* Experiencias motivacionales
* Diseño y desarrollo de producto
* Cosas y lugares inteligentes
* Pensamiento estratégico y de diseño
* Transformación y agilidad


***Presencia***

:earth_americas: [Facebook](https://www.facebook.com/aviadamx)

:earth_americas: [YouTube](https://www.youtube.com/channel/UCDtOIqWxKHTdtmVi8yr_D7Q)

:earth_americas: [Instagram](https://www.instagram.com/luxoft_global/)

:earth_americas: [Twitter](https://twitter.com/Luxoft)

:earth_americas: [Linkedin](https://www.linkedin.com/company/luxoft/)


***Ofertas laborales***

Se puede aplicar para loborar en la empresa en  http://career.luxoft.com/ O https://career.luxoft.com/locations/mexico/


***Blog de Ingeniería***

Para ingresar al [Blog](https://career.luxoft.com/events/) :point_left:


***Tecnologías***
* Integración, automatización, RPA, BPA, ML y AI
* SQL Server (SSIS, SSRS)
* Python
* Java
* Angular
* MySQL


----





---

![Logotipo](imagenes/svitla.PNG)
## :computer: Svitla Systems

***Sitio web***

:point_right: [Svitla Systems](https://svitla.com/)


***Ubicación***:mag:

Dirección: Colonias #221 3rd floor, 44160, Guadalajara, Jalisco


:point_right: [Google Maps](https://www.google.com.mx/maps/search/Colonias+%23221+3rd+floor,+44160,+Guadalajara,+Jalisco/@20.6727801,-103.3679756,17z/data=!3m1!4b1?authuser=1) 


***Acerca de***:memo: 

Svitla® es una empresa probada de desarrollo de software personalizado y un proveedor de pruebas con sede en Silicon Valley. Somos su conducto hacia las innovaciones tecnológicas más punteras. Ofrecemos un valor incomparable a nuestros clientes, que confían en nuestra experiencia y muchos años de experiencia en Managed Team Extension y AgileSquads.


***Servicios***

* Desarrollo web, soluciones
* Outsourcing de TI, equipo de desarrollo dedicado
* Big Data, Internet de las cosas
* Desarrollo web, desarrollo móvil y eCommerce
* Desarrollo de software
* Machine Learning


***Presencia***

:earth_americas: [Facebook](https://www.facebook.com/SvitlaSystems)

:earth_americas: [YouTube](https://www.youtube.com/channel/UC1nu2LV4_08GoZThHEindWA)

:earth_americas: [Instagram](https://www.instagram.com/svitlasystems/)

:earth_americas: [Twitter](https://twitter.com/SvitlaSystemsIn)

:earth_americas: [Linkedin](https://www.linkedin.com/company/svitla-systems-inc-/?trk=biz-companies-cym)


***Ofertas laborales***

Se puede aplicar para loborar en la empresa en https://svitla.com/career


***Blog de Ingeniería***

Para ingresar al [Blog](https://svitla.com/blog) :point_left:


***Tecnologías***

* Java
* Ruby 
* C# 
* PHP 
* Python 
* JavaScript
* HTML
* CSS
* Oracle
* SQL 
* MongoDB 
* Redis 
* Cassandra
* Node.js

----
